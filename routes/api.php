<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\WalletController;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('client', [ClientController::class, 'save']);
Route::any('user', function () {

    $server = new \nusoap_server();

    $server->configureWSDL('UsersService', false, url('user'));

    //Estructura del input del servicio
    $server->wsdl->addComplexType(
        "inputsUser",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'name' => array('name' => 'name', 'type' => 'xsd:string'),
            'email' => array('name' => 'email', 'type' => 'xsd:string'),
            'document' => array('name' => 'document', 'type' => 'xsd:int'),
            'phone' => array('name' => 'document', 'type' => 'xsd:int'),
        )
    );

    //Estructura output del servicio
    $server->wsdl->addComplexType(
        "outputUser",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'success' => array('name' => 'success', 'type' => 'xsd:boolean'),
            'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
            'message_error' => array('name' => 'message_error', 'type' => 'xsd:string'),
        )
    );
    $server->register('insertUser',
        array("inputsUser" => "tns:inputsUser"),
        array('outputUser' => 'tns:outputUser'),
    );

    function insertUser($request){
        try {
            $clientController = new ClientController();
            return $clientController->store($request);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    $POST_DATA = file_get_contents("php://input");
    return \Response::make($server->service($POST_DATA), 200, array('Content-Type' => 'text/xml; charset=ISO-8859-1'));
});


Route::post('recharge', [WalletController::class, 'save']);
Route::post('balance', [WalletController::class, 'credit']);
Route::any('whallet', function () {
    $server = new \nusoap_server();

    $server->configureWSDL('WalletsService', false, url('whallet'));

    //Estructura del input del servicio
    $server->wsdl->addComplexType(
        "inputsWallet",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'amount' => array('name' => 'amount', 'type' => 'xsd:double'),
            'document' => array('name' => 'document', 'type' => 'xsd:int'),
            'phone' => array('name' => 'document', 'type' => 'xsd:string'),
        )
    );

    //Estructura output del servicio
    $server->wsdl->addComplexType(
        "outputWallet",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'success' => array('name' => 'success', 'type' => 'xsd:boolean'),
            'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
            'message_error' => array('name' => 'message_error', 'type' => 'xsd:string'),
        )
    );

    $server->register('insertWallet',
        array("inputsWallet" => "tns:inputsWallet"),
        array('outputWallet' => 'tns:outputWallet'),
    );

    function insertWallet($request){
        try {
            $walletController = new WalletController();
            return $walletController->store($request);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    //Estructura del input de consulta de saldo
    $server->wsdl->addComplexType(
        "inputsBalance",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'document' => array('name' => 'document', 'type' => 'xsd:int'),
            'phone' => array('name' => 'document', 'type' => 'xsd:string'),
        )
    );

    //Estructura output de consulta de saldo
    $server->wsdl->addComplexType(
        "outputBalance",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'success' => array('name' => 'success', 'type' => 'xsd:boolean'),
            'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
            'balance' => array('name' => 'balance', 'type' => 'xsd:double'),
            'message_error' => array('name' => 'message_error', 'type' => 'xsd:string'),
        )
    );

    $server->register('balanceWallet',
        array("inputsBalance" => "tns:inputsBalance"),
        array('outputBalance' => 'tns:outputBalance'),
    );

    function balanceWallet($request){
        try {
            $walletController = new WalletController();
            return $walletController->balance($request);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    $POST_DATA = file_get_contents("php://input");
    return \Response::make($server->service($POST_DATA), 200, array('Content-Type' => 'text/xml; charset=ISO-8859-1'));
});


Route::post('pay', [PaymentController::class, 'pay']);
Route::post('confirm-pay', [PaymentController::class, 'confirmPay']);
Route::any('payment', function () {

    $server = new \nusoap_server();

    $server->configureWSDL('PaymentsService', false, url('payment'));

    //Estructura del input del servicio
    $server->wsdl->addComplexType(
        "inputsPay",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'client' => array('name' => 'client', 'type' => 'xsd:int'),
            'amount' => array('name' => 'amount', 'type' => 'xsd:double'),
            'dataTypeToClient' => array('name' => 'dataTypeToClient', 'type' => 'xsd:string'),
            'payToClient' => array('name' => 'payToClient', 'type' => 'xsd:string'),
        )
    );

    //Estructura output del servicio
    $server->wsdl->addComplexType(
        "outputPay",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'success' => array('name' => 'success', 'type' => 'xsd:boolean'),
            'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
            'message_error' => array('name' => 'message_error', 'type' => 'xsd:string'),
        )
    );
    $server->register('insertPay',
        array("inputsPay" => "tns:inputsPay"),
        array('outputPay' => 'tns:outputPay'),
    );

    function insertPay($request){
        try {
            $paymentController = new PaymentController();
            return $paymentController->store($request);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    //Estructura del input del servicio
    $server->wsdl->addComplexType(
        "inputsProcessedPay",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'client' => array('name' => 'client', 'type' => 'xsd:int'),
            'code' => array('name' => 'code', 'type' => 'xsd:int'),
        )
    );

    //Estructura output del servicio
    $server->wsdl->addComplexType(
        "outputProcessedPay",
        "complexType",
        "struct",
        "all",
        "",
        array(
            'success' => array('name' => 'success', 'type' => 'xsd:boolean'),
            'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
            'message_error' => array('name' => 'message_error', 'type' => 'xsd:string'),
        )
    );
    $server->register('processedPay',
        array("inputsProcessedPay" => "tns:inputsProcessedPay"),
        array('outputProcessedPay' => 'tns:outputProcessedPay'),
    );

    function processedPay($request){
        try {
            $paymentController = new PaymentController();
            return $paymentController->processedPayment($request);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    $POST_DATA = file_get_contents("php://input");
    return \Response::make($server->service($POST_DATA), 200, array('Content-Type' => 'text/xml; charset=ISO-8859-1'));
});
