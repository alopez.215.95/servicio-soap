<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreClientRequest;
use App\Models\Client;
use App\Models\Wallet;
use DOMDocument;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SimpleXMLElement;
use XMLWriter;

class WalletController extends Controller
{
    public function save(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('amount',     $request->get('amount'));
        $writer->writeElement('document',   $request->get('document'));
        $writer->writeElement('phone',      $request->get('phone'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <insertWallet soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsWallet xsi:type="wal:inputsWallet" xmlns:wal="http://backendsoap.test/soap/WalletsService">
                        <!--You may enter the following 3 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsWallet>
                </insertWallet>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        try {
            $ch = curl_init();

            curl_setopt_array($ch, [
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_URL            => 'http://backendsoap.test/api/whallet?wsdl',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FAILONERROR    => true,
                CURLOPT_TIMEOUT        => 120,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $xml,
                CURLOPT_HTTPHEADER     => [
                    "Content-Type: text/xml;charset=\"utf-8\"",
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    'SOAPAction: ""',
                    "Content-Length: " . strlen($xml),
                    // "Authorization: b0NfsM1S9oTItYFqNkDzREi0pSoP0Ukp"
                ],
            ]);

            $response = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);

            if ($error) {
                throw new Exception($error);
            }

            return $response;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    public function store($request)
    {

        $rules = [
            'amount' => 'required',
            'document' => 'required|exists:App\Models\Client,document',
            'phone' => 'required|exists:App\Models\Client,phone',
        ];
        $messages = [
            'amount.required' => 'Amount is required.',
            'document.required' => 'Document is required.',
            'document.exists' => 'The Document must be registered.',
            'phone.required' => 'Phone is required.',
            'phone.exists' => 'The Phone must be registered.',
        ];
        $validator = Validator::make($request, $rules, $messages);
        if ($validator->fails()) {
            return [
                'success' => false,
                'cod_error' => 1,
                'message_error' => $validator->errors()
            ];
        }

        $validated = $validator->validated();

        $client = Client::where([
            'document' => $validated['document'],
            'phone' => $validated['phone'],
        ])->first();

        try {

            $wallet = Wallet::where('client_id', $client->id)->get();
            if ($wallet->count()) {
                $wallet = $wallet->first();

                $balance = $wallet->balance + $validated['amount'];
                $wallet->balance = $balance;

                $wallet = $wallet->save();

            } else {

                $wallet = new Wallet();

                $wallet->client_id = $client->id;
                $wallet->balance = $validated['amount'];

                $wallet = $wallet->save();
            }

            return [
                'success' => true,
                'cod_error' => '00',
                'message_error' => '-',
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    public function credit(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('document',   $request->get('document'));
        $writer->writeElement('phone',      $request->get('phone'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <balanceWallet soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsBalance xsi:type="wal:inputsBalance" xmlns:wal="http://backendsoap.test/soap/WalletsService">
                        <!--You may enter the following 2 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsBalance>
                </balanceWallet>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        try {
            $ch = curl_init();

            curl_setopt_array($ch, [
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_URL            => 'http://backendsoap.test/api/whallet?wsdl',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FAILONERROR    => true,
                CURLOPT_TIMEOUT        => 120,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $xml,
                CURLOPT_HTTPHEADER     => [
                    "Content-Type: text/xml;charset=\"utf-8\"",
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    'SOAPAction: ""',
                    "Content-Length: " . strlen($xml),
                    // "Authorization: b0NfsM1S9oTItYFqNkDzREi0pSoP0Ukp"
                ],
            ]);

            $response = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);

            if ($error) {
                throw new Exception($error);
            }

            return $response;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    public function balance($request)
    {

        $rules = [
            'document' => 'required|exists:App\Models\Client,document',
            'phone' => 'required|exists:App\Models\Client,phone',
        ];
        $messages = [
            'document.required' => 'Document is required.',
            'document.exists' => 'The Document must be registered.',
            'phone.required' => 'Phone is required.',
            'phone.exists' => 'The Phone must be registered.',
        ];
        $validator = Validator::make($request, $rules, $messages);
        if ($validator->fails()) {
            return [
                'success' => false,
                'cod_error' => 1,
                'message_error' => $validator->errors()
            ];
        }

        $validated = $validator->validated();

        $client = Client::where([
            'document' => $validated['document'],
            'phone' => $validated['phone'],
        ])->get();

        if ($client->count() == 0) {
            return [
                'success' => false,
                'cod_error' => '02',
                'message_error' => 'Error! The phone and the document do not match.',
            ];

        } else {
            $client = $client->first();
        }

        try {

            $wallet = Wallet::where('client_id', $client->id)->get();

            if ($wallet->count()) {
                $wallet = $wallet->first();

                return [
                    'success' => true,
                    'cod_error' => '00',
                    'balance' => $wallet->balance,
                    'message_error' => '-',
                ];
            } else {
                return [
                    'success' => false,
                    'cod_error' => '00',
                    'balance' => 0,
                    'message_error' => 'Client currently has no funds.',
                ];
            }

        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }
}
