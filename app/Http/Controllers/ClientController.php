<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreClientRequest;
use App\Models\Client;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use XMLWriter;

class ClientController extends Controller
{
    public function save(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('name',       $request->get('name'));
        $writer->writeElement('email',      $request->get('email'));
        $writer->writeElement('document',   $request->get('document'));
        $writer->writeElement('phone',      $request->get('phone'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <insertUser soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsUser xsi:type="user:inputsUser" xmlns:user="http://backendsoap.test/soap/UsersService">
                        <!--You may enter the following 4 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsUser>
                </insertUser>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        try {
            $ch = curl_init();

            curl_setopt_array($ch, [
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_URL            => 'http://backendsoap.test/api/user?wsdl',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FAILONERROR    => true,
                CURLOPT_TIMEOUT        => 120,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $xml,
                CURLOPT_HTTPHEADER     => [
                    "Content-Type: text/xml;charset=\"utf-8\"",
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    'SOAPAction: ""',
                    "Content-Length: " . strlen($xml),
                    // "Authorization: b0NfsM1S9oTItYFqNkDzREi0pSoP0Ukp"
                ],
            ]);

            $response = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);

            if ($error) {
                throw new Exception($error);
            }

            return $response;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    public function store($request)
    {

        $rules = [
            'name' => 'required',
            'email' => 'required|unique:clients',
            'document' => 'required|unique:clients',
            'phone' => 'required|unique:clients',
        ];
        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'document.required' => 'Document is required.',
            'phone.required' => 'Phone is required.',
            'email.unique' => 'Email must be unique.',
            'document.unique' => 'Document must be unique.',
            'phone.unique' => 'Phone must be unique.',
        ];
        $validator = Validator::make($request, $rules, $messages);
        if ($validator->fails()) {
            return [
                'success' => false,
                'cod_error' => 1,
                'message_error' => $validator->errors()
            ];
        }

        $validated = $validator->validated();

        try {

            $user = new Client($validated);
            $user->save();

            return [
                'success' => true,
                'cod_error' => '00',
                'message_error' => '-',
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }
}
