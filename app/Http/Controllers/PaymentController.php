<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreClientRequest;
use App\Mail\sendEmail;
use App\Models\Client;
use App\Models\Payment;
use App\Models\Wallet;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use XMLWriter;

class PaymentController extends Controller
{
    public function pay(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('client',             $request->get('client'));
        $writer->writeElement('amount',             $request->get('amount'));
        $writer->writeElement('dataTypeToClient',   $request->get('dataTypeToClient'));
        $writer->writeElement('payToClient',        $request->get('payToClient'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <insertPay soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsPay xsi:type="pay:inputsPay" xmlns:pay="http://backendsoap.test/soap/PaymentsService">
                        <!--You may enter the following 4 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsPay>
                </insertPay>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        try {
            $ch = curl_init();

            curl_setopt_array($ch, [
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_URL            => 'http://backendsoap.test/api/payment?wsdl',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FAILONERROR    => true,
                CURLOPT_TIMEOUT        => 120,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $xml,
                CURLOPT_HTTPHEADER     => [
                    "Content-Type: text/xml;charset=\"utf-8\"",
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    'SOAPAction: ""',
                    "Content-Length: " . strlen($xml),
                    // "Authorization: b0NfsM1S9oTItYFqNkDzREi0pSoP0Ukp"
                ],
            ]);

            $response = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);

            if ($error) {
                throw new Exception($error);
            }

            return $response;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    public function store($request)
    {

        $rules = [
            'client' => 'required',
            'amount' => 'required',
            'dataTypeToClient' => 'required',
            'payToClient' => 'required',
        ];
        $messages = [
            'client.required' => 'Customer who will pay, is required.',
            'amount.required' => 'Amount is required.',
            'dataTypeToClient.required' => 'Type of data to pay the customer, is required.',
            'payToClient.required' => 'Email, document or phone to pay the customer, is required.',
        ];
        $validator = Validator::make($request, $rules, $messages);
        if ($validator->fails()) {
            return [
                'success' => false,
                'cod_error' => '01',
                'message_error' => $validator->errors()
            ];
        }

        $validated = $validator->validated();

        $client = null;
        $client = Client::where([
            'id' => $validated['client'],
        ])->get();
        if ($client and $client->count() == 0) {
            return [
                'success' => false,
                'cod_error' => '02',
                'message_error' => 'The client is invalid or does not exist.'
            ];
        }
        $client = $client->first();

        $clientWallet = Wallet::where('client_id', $client->id)->get();
        if ($clientWallet->count() == 0) {
            return [
                'success' => false,
                'cod_error' => '03',
                'message_error' => 'The paying customer has no funds.'
            ];
        }
        $clientWallet = $clientWallet->first();

        if ((double)$validated['amount'] > (double)$clientWallet->balance) {
            return [
                'success' => false,
                'cod_error' => '04',
                'message_error' => 'The amount to be paid is greater than your funds.'
            ];
        }

        $payToClient = null;
        switch ($validated['dataTypeToClient']) {
            case 'email':
                $payToClient = Client::where([
                    'email' => $validated['payToClient'],
                ])->get();
                break;

            case 'document':
                $payToClient = Client::where([
                    'document' => $validated['payToClient'],
                ])->get();
                break;

            case 'phone':
                $payToClient = Client::where([
                    'phone' => $validated['payToClient'],
                ])->get();
                break;

            default:
                return [
                    'success' => false,
                    'cod_error' => '05',
                    'message_error' => 'The '.$validated['dataTypeToClient']. ' is invalid, send "email", "document" or "phone".'
                ];
                break;
        }
        $payToClient = $payToClient->first();

        try {

            $pay = new Payment();
            $pay->client_id = $client->id;
            $pay->amount = (double)$validated['amount'];
            $pay->for_client_id = $payToClient->id;

            $otp = rand(100000,999999);
            $pay->opt = $otp;
            $pay = $pay->save();

            if (!$pay) {
                return [
                    'success' => false,
                    'cod_error' => '06',
                    'message_error' => 'An error occurred while registering the payment.'
                ];
            }

            $mail_details = [
                'subject' => 'Confirm the payment',
                'body' => 'Your confirmation number is: '. $otp
            ];
            Mail::to($client->email)->send(new sendEmail($mail_details));

            return [
                'success' => true,
                'cod_error' => '00',
                'message_error' => 'Your payment is being processed, please confirm your payment with the code sent to your email.',
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    public function confirmPay(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('client', $request->get('client'));
        $writer->writeElement('code',   $request->get('code'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <processedPay soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsProcessedPay xsi:type="pay:inputsProcessedPay" xmlns:pay="http://backendsoap.test/soap/PaymentsService">
                        <!--You may enter the following 2 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsProcessedPay>
                </processedPay>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        try {
            $ch = curl_init();

            curl_setopt_array($ch, [
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_URL            => 'http://backendsoap.test/api/payment?wsdl',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FAILONERROR    => true,
                CURLOPT_TIMEOUT        => 120,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $xml,
                CURLOPT_HTTPHEADER     => [
                    "Content-Type: text/xml;charset=\"utf-8\"",
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    'SOAPAction: ""',
                    "Content-Length: " . strlen($xml),
                    // "Authorization: b0NfsM1S9oTItYFqNkDzREi0pSoP0Ukp"
                ],
            ]);

            $response = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);

            if ($error) {
                throw new Exception($error);
            }

            return $response;
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }

    public function processedPayment($request)
    {
        $rules = [
            'client' => 'required',
            'code' => 'required',
        ];
        $messages = [
            'client.required' => 'Customer who will pay, is required.',
            'code.required' => 'Confirmation code is required.',
        ];
        $validator = Validator::make($request, $rules, $messages);
        if ($validator->fails()) {
            return [
                'success' => false,
                'cod_error' => '01',
                'message_error' => $validator->errors()
            ];
        }

        $validated = $validator->validated();

        $client = null;
        $client = Client::where([
            'id' => $validated['client'],
        ])->get();
        if ($client and $client->count() == 0) {
            return [
                'success' => false,
                'cod_error' => '02',
                'message_error' => 'The client is invalid or does not exist.'
            ];
        }

        try {
            $client = $client->first();

            //Consulta la wallet del cliente que pago
            $clientWallet = Wallet::where('client_id', $client->id)->get();
            if ($clientWallet->count() == 0) {
                return [
                    'success' => false,
                    'cod_error' => '03',
                    'message_error' => 'The paying customer has no funds.'
                ];
            }
            $clientWallet = $clientWallet->first();

            //Consulta del pago realizado
            $pay = Payment::where(['client_id' => $client->id, 'opt' => $validated['code']])->get();
            if ($pay->count() == 0) {
                return [
                    'success' => false,
                    'cod_error' => '04',
                    'message_error' => 'The payment does not exist or the code is invalid.'
                ];
            }
            $pay = $pay->first();
            if ($pay->processed_payment) {
                return [
                    'success' => false,
                    'cod_error' => '05',
                    'message_error' => 'This payment has been made and previously confirmed.'
                ];
            }

            //actualizacion de la wallet del cliente que pago
            $balance = $clientWallet->balance - $pay->amount;
            $clientWallet->balance = $balance;
            $clientWallet = $clientWallet->save();

            //actualizacion de la wallet del cliente que recibio el pago
            $payToClientWallet = Wallet::where('client_id', $pay->for_client_id)->get();

            if ($payToClientWallet->count()) {
                $payToClientWallet = $payToClientWallet->first();

                $balance = $payToClientWallet->balance + $pay->amount;
                $payToClientWallet->balance = $balance;

                $payToClientWallet = $payToClientWallet->save();

            } else {
                $payToClientWallet = new Wallet();

                $payToClientWallet->client_id = $pay->for_client_id;
                $payToClientWallet->balance = $pay->amount;

                $payToClientWallet = $payToClientWallet->save();
            }

            $pay->processed_payment = TRUE;

            $payToClient = Client::where([
                'id' => $pay->for_client_id,
            ])->get()->first();

            $pay = $pay->save();

            $mail_details = [
                'subject' => 'Your payment has been confirmed.',
                'body' => 'Congratulations, enjoy your purchase.'
            ];
            Mail::to($payToClient->email)->send(new sendEmail($mail_details));

            return [
                'success' => true,
                'cod_error' => '00',
                'message_error' => 'Congratulations, payment confirmed.',
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'cod_error' => $e->getCode(),
                'message_error' => $e->getMessage()
            ];
        }
    }
}
