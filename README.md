Pasos para la instalacion
1.- Abrir terminal donde desea descargar el proyecto.
2.- Clonar proyecto ejecutando git clone git@gitlab.com:alopez.215.95/servicio-soap.git
3.- Entrar al directorio raiz del proyecto.
4.- Ejecutar el comando 'composer install' en su terminal.
5.- Crear el archivo .env a partir de .env.example.
6.- Crear base de datos en mysql con el nombre 'backendsoap'.
7.- Ejecutar el comando 'php artisan key:generate' en su terminal.
8.- Ejecutar el comando 'php artisan migrate --seed' en su terminal.
9.- Ya puede iniciar el servidor
CONSULTAS
*Ojo: Puede cambiar backendsoap.test por localhost:8000 o el puerto que cargue al ejecutar el proyecto
Tambien para probar desde el mismo proyecto debe descomentar en los controladores la linea de authorization


Este proyeco provee datos para el proyecto del repositorio https://gitlab.com/alopez.215.95/servicio-rest-consumo-de-soap 


Tambien adjunto la coleccion para ver como funciona y se consumen los recursos
