<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $user */
        $user = User::create([
            'name' => 'Super-Admin User',
            'email' => 'superadmin@test.com',
            'password' => bcrypt('admin123'),
        ]);

        $this->command->line('finish insert SuperAdmin Role');
    }
}
